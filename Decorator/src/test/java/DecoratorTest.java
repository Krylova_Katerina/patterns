import decorator.ru.omsu.FirstBuilder;
import decorator.ru.omsu.Sequence;
import decorator.ru.omsu.SortClient;
import decorator.ru.omsu.BuilderException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static decorator.ru.omsu.Proxy.Proxy.isBuilder;
import static decorator.ru.omsu.facade.Facade.doBuild;
import static org.junit.Assert.fail;
import static org.testng.AssertJUnit.assertEquals;

public class DecoratorTest {

    @Test
    public void DecorateTest() {
        int[] arr = new int[]{3, 2, 1, 4, 8};
        int[] arr2 = new int[]{5, 4, 2, 1, 4};
        SortClient sortClient = new SortClient();
        sortClient.setStrategyBuilder(new FirstBuilder());
        Sequence result = sortClient.buildSequenceStrategy(arr, arr2);

        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(2);
        list.add(4);
        list.add(4);
        list.add(4);
        list.add(8);
        Sequence expect = new Sequence(list);

        assertEquals(result, expect);
    }


    @Test
    public void FacadeTest() {
        try {
            int[] arr = new int[]{3, 2, 1, 4, 8};
            int[] arr2 = new int[]{5, 4, 2, 1, 4};

            Sequence result = doBuild(arr, arr2, 1);

            List<Integer> list = new ArrayList<>();
            list.add(2);
            list.add(2);
            list.add(4);
            list.add(4);
            list.add(4);
            list.add(8);
            Sequence expect = new Sequence(list);

            assertEquals(result, expect);
        } catch (BuilderException e) {
            fail("BuilderException");

        }
    }

        @Test
        public void ProxyTest() {
            try {
                int[] arr = new int[]{3, 2, 1, 4, 8};
                int[] arr2 = new int[]{5, 4, 2, 1, 4};

                Sequence result = isBuilder(arr, arr2, 1);

                List<Integer> list = new ArrayList<>();
                list.add(2);
                list.add(2);
                list.add(4);
                list.add(4);
                list.add(4);
                list.add(8);
                Sequence expect = new Sequence(list);

                assertEquals(result, expect);
            } catch(BuilderException e) {
                fail("BuilderException");

            }
    }

    @Test(expected = BuilderException.class)
    public void ProxyTest2() throws BuilderException {

            int[] arr = new int[]{3, 2, 1, 4, 8};
            int[] arr2 = new int[]{5, 4, 2, 1, 4};

            Sequence result = isBuilder(arr, arr2, 5);


    }


}
