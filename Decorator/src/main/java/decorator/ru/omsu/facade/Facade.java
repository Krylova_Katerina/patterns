package decorator.ru.omsu.facade;

import decorator.ru.omsu.*;

public class Facade {

    public static Sequence doBuild(int [] arr, int [] arr2, int id) throws BuilderException {
        SortClient sortClient = new SortClient();

        if(id == 1){
            sortClient.setStrategyBuilder(new FirstBuilder());
        }else if (id ==2){
            sortClient.setStrategyBuilder(new SecondBuilder());
        }else{
            sortClient.setStrategyBuilder(new SecondBuilder());


        }
        return sortClient.buildSequenceStrategy(arr, arr2);
    }
}
