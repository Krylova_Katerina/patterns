package decorator.ru.omsu.Proxy;

import decorator.ru.omsu.BuilderException;
import decorator.ru.omsu.Sequence;

import static decorator.ru.omsu.facade.Facade.doBuild;

public class Proxy {
    public static Sequence isBuilder(int[] arr, int[] arr2, int id) throws BuilderException {
        if (!((id>0)&&(id<4))){
            throw new BuilderException("no builder");
        }

        return  doBuild(arr,arr2, id);
    }
}
