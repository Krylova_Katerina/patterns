package decorator.ru.omsu.decorator;

import decorator.ru.omsu.Sequence;

public class SourceDecorator implements Sourse {
    protected EvenSequence evenSequence= new EvenSequence();
    @Override
    public Sequence makeSomeSequence(Sequence sequence) {
        return evenSequence.makeSomeSequence(sequence);
    }


}
