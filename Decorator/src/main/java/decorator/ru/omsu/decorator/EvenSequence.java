package decorator.ru.omsu.decorator;


import decorator.ru.omsu.Sequence;

import java.util.ArrayList;
import java.util.List;

public class EvenSequence implements Sourse {
    public EvenSequence() {
    }

    @Override
    public Sequence makeSomeSequence(Sequence sequence) {
        List<Integer> list = new ArrayList<>();
        for (int i=0; i<sequence.getList().size(); i++){
            if (sequence.getList().get(i)%2==0){
                list.add(sequence.getList().get(i));
            }
        }
      return new Sequence(list);
    }
}
