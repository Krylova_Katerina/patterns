package decorator.ru.omsu.decorator;

import decorator.ru.omsu.Sequence;

public interface Sourse {
     Sequence makeSomeSequence(Sequence sequence);
}
