package decorator.ru.omsu;;
import decorator.ru.omsu.Sort.*;

public class ThirdBuilder extends Builder implements StrategyBuilder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        PastSort past=new PastSort();
        arr= past.beSort(arr);

        BucketSort bucket = new BucketSort();
        arr2= bucket.beSort(arr2);

        Sequence result = createNewSequenceProduct(arr,arr2);
        return result;
    }


}