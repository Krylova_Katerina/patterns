package decorator.ru.omsu;

import decorator.ru.omsu.decorator.SourceDecorator;

/**
 * Created by User on 09.09.2017.
 */
public class SortClient {
    private StrategyBuilder strategyBuilder;
    protected SourceDecorator sourceDecorator = new SourceDecorator();


    public void setStrategyBuilder(StrategyBuilder strategyBuilder) {
        this.strategyBuilder = strategyBuilder;
    }


    public Sequence buildSequenceStrategy(int[] a, int[] b) {
         Sequence sequence = strategyBuilder.buildSequence(a, b);
         return sourceDecorator.makeSomeSequence(sequence);
    }

    public StrategyBuilder getStrategyBuilder() {
        return strategyBuilder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SortClient that = (SortClient) o;

        return strategyBuilder != null ? strategyBuilder.equals(that.strategyBuilder) : that.strategyBuilder == null;
    }

    @Override
    public int hashCode() {
        return strategyBuilder != null ? strategyBuilder.hashCode() : 0;
    }
}
