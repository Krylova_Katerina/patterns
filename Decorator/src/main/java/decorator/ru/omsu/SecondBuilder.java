package decorator.ru.omsu;


import decorator.ru.omsu.Sort.*;


public class SecondBuilder extends Builder implements StrategyBuilder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        PastSort past=new PastSort();
        arr= past.beSort(arr);

        FastSort fast = new FastSort();
        arr2 = fast.beSort(arr2);

        Sequence result = createNewSequenceProduct(arr,arr2);
        return result;
    }


}