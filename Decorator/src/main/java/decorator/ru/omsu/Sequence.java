package decorator.ru.omsu;

import java.util.List;

/**
 * Created by User on 09.09.2017.
 */
public class Sequence {

    private List<Integer> list;

    public Sequence(List<Integer> list) {
        this.list = list;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sequence sequence = (Sequence) o;

        return list != null ? list.equals(sequence.list) : sequence.list == null;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }
}
