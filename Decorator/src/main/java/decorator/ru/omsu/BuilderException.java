package decorator.ru.omsu;

public class BuilderException extends Throwable {
    public BuilderException(String message) {
        super(message);
    }
}
