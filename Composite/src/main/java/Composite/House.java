package Composite;

public class House extends Builder {
    private String house;

    public House(String house) {
        this.house = house;
        System.out.println("I built a house "+ house);
    }

    public String getHouse() {
        return house;
    }

}
