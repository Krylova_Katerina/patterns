package Composite;

public class Client {

    public static void main(String[] args) throws BuilderException {
        BuilderDirector builderDirector=new BuilderDirector();
        Street street = (Street) BuilderDirector.buildSomething("Street", "Lenin Street",
                                 builderDirector.buildSomething("House", "House1"),
                                 builderDirector.buildSomething("House", "House1"),
                                 builderDirector.buildSomething("House", "House1"));

        Street street1= (Street) builderDirector.buildSomething("Street", "World Street",
                                 builderDirector.buildSomething("House", "House5"),
                                 builderDirector.buildSomething("House", "House6"),
                                 builderDirector.buildSomething("House", "House8"));

        City city= (City) builderDirector.buildSomething("City", "Beautiful City", street, street1);


    }
}
