package Composite;

public class BuilderDirector {
    public static Builder buildSomething(String id, String builderName, Builder... builders)throws BuilderException {
        String idRet = "";
        switch (id) {
            case "House":
                idRet = "House";
                return new House(builderName);

            case "Street":
                idRet = "Street";
                return new Street(builderName, builders);

            case "City":
                idRet = "City";
                return new City(builderName, builders);

        }
        if (idRet.equals("")) { throw new BuilderException("This is not the builder");}
        return new House(builderName);
    }
}
