package Composite;

import java.util.ArrayList;
import java.util.List;

public abstract class Builder {
 protected List<Builder> builder = new ArrayList<>();

    public List<Builder> getBuilder() {
        return builder;
    }

    public Builder getBuilderValue(int i){
       return builder.get(i);
    }

    public int getBuilderSize(){
        return builder.size();
    }

    public void addBuilder(Builder value){
        builder.add(value);
    }

    public void append(Builder... value){
        for (Builder part : value) {
            builder.add(part);
        }

    }
}
