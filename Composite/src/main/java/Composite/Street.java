package Composite;

public class Street extends Builder {

    public Street(String nameStreet, Builder... value){
        for (Builder builder1 : value) {
          this.builder.add(builder1);
        }
        System.out.println("I built a street "+ nameStreet);

       }
    }



