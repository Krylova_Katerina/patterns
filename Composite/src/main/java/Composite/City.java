package Composite;

public class City extends Street {
    public City(String cityName, Builder... value) {
        super(cityName, value);
        System.out.println("I built a city "+cityName);
    }

 /* public List<Builder> getBuilderValue(){
        return super.getBuilder();
  }*/

}
