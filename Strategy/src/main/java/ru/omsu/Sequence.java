package ru.omsu;

import java.util.Arrays;

/**
 * Created by User on 09.09.2017.
 */
public class Sequence {
    private int[]arr;

    public Sequence(int[] arr) {
        this.arr = arr;
    }


    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sequence sequence = (Sequence) o;

        return Arrays.equals(arr, sequence.arr);

    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arr);
    }
}
