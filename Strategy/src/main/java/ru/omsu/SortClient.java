package ru.omsu;

/**
 * Created by User on 09.09.2017.
 */
public class SortClient {
    private StrategyBuilder strategyBuilder;


    public void setStrategyBuilder(StrategyBuilder strategyBuilder) {
        this.strategyBuilder = strategyBuilder;
    }


    public Sequence buildSequenceStrategy(int[] a, int[] b) {
        return strategyBuilder.buildSequence(a, b);
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SortClient that = (SortClient) o;

        return strategyBuilder != null ? strategyBuilder.equals(that.strategyBuilder) : that.strategyBuilder == null;
    }

    @Override
    public int hashCode() {
        return strategyBuilder != null ? strategyBuilder.hashCode() : 0;
    }
}
