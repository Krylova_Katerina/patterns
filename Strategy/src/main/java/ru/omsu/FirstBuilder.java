package ru.omsu;

import ru.omsu.Sort.BucketSort;
import ru.omsu.Sort.FastSort;

/**
 * Created by User on 10.09.2017.
 */
public class FirstBuilder extends Builder implements StrategyBuilder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        BucketSort bucket = new BucketSort();
        arr= bucket.beSort(arr);

        FastSort fast = new FastSort();
        arr2 = fast.beSort(arr2);


       Sequence result = createNewSequenceProduct(arr,arr2);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


}
