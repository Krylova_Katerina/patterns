package ru.omsu;


import ru.omsu.Sort.FastSort;
import ru.omsu.Sort.PastSort;

public class SecondBuilder extends Builder implements StrategyBuilder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        PastSort past=new PastSort();
        arr= past.beSort(arr);

        FastSort fast = new FastSort();
        arr2 = fast.beSort(arr2);

        Sequence result = createNewSequenceProduct(arr,arr2);
        return result;
    }


}