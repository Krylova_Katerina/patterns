package ru.omsu;

import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;

public class SingletonTest {


    @Test
    public void StrategyTest(){
        int[] arr= new int[]{3, 2, 1, 4, 8};
        int[] arr2= new int[]{5, 4, 2, 1, 4};
        SortClient sortClient= new SortClient();
        sortClient.setStrategyBuilder(new FirstBuilder());
        Sequence result = sortClient.buildSequenceStrategy(arr, arr2);
        Sequence expect = new Sequence(new int[]{1, 1, 2, 2, 3, 4, 4, 4, 5, 8});
        assertEquals(result,expect);

    }

    @Test
    public void Strategy2Test(){
        int[] arr= new int[]{3, 2, 1, 4, 8};
        int[] arr2= new int[]{5, 4, 2, 1, 4};
        SortClient sortClient= new SortClient();
        sortClient.setStrategyBuilder(new SecondBuilder());
        Sequence result = sortClient.buildSequenceStrategy(arr, arr2);
        Sequence expect = new Sequence(new int[]{1, 1, 2, 2, 3, 4, 4, 4, 5, 8});
        assertEquals(result,expect);

    }


    @Test
    public void Strategy3Test(){
        int[] arr= new int[]{3, 2, 1, 4, 8};
        int[] arr2= new int[]{5, 4, 2, 1, 4};
        SortClient sortClient= new SortClient();
        sortClient.setStrategyBuilder(new ThirdBuilder());
        Sequence result = sortClient.buildSequenceStrategy(arr, arr2);
        Sequence expect = new Sequence(new int[]{1, 1, 2, 2, 3, 4, 4, 4, 5, 8});
        assertEquals(result,expect);

    }








}
