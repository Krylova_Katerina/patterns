package ru.omsu;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static ru.omsu.BuilderDirector.getInstances;

public class SingletonTest {


    @Test
    public void getInstanceTest(){
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);

        BuilderDirector builderDirector = getInstances(sequence);

        list.add(14);
        Sequence sequence1= new Sequence(list);

        BuilderDirector builderDirector1= getInstances(sequence1);

        assertEquals(builderDirector,builderDirector1);

    }


    @Test
    public void getInstanceTest2(){
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(5);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);

        BuilderDirector builderDirector = getInstances(sequence);

        list.add(14);
        list.add(5);
        Sequence sequence1= new Sequence(list);

        BuilderDirector builderDirector1= getInstances(sequence1);

        assertEquals(builderDirector,builderDirector1);

    }




}
