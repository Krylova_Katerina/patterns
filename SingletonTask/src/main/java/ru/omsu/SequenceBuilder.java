
package ru.omsu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 15.09.17.
 */
public abstract class SequenceBuilder {
    private Sequence sequence;

    public Sequence getSequence() {
        return sequence;
    }

    public Sequence createNewSequenceProduct(int[] arr, int[] arr2) {
        int[] result = new int[arr.length + arr2.length];
        int i = 0, j = 0, index = 0;
        while (i < arr.length && j < arr2.length) {
            if (arr[i] < arr2[j]) {
                result[index] = arr[i];
                i++;
            } else {
                result[index] = arr2[j];
                j++;
            }
            index++;
        }
        while (i < arr.length) {
            result[index] = arr[i];
            index++;
            i++;
        }
        while (j < arr2.length) {
            result[index] = arr2[j];
            index++;
            j++;
        }


        List<Integer> result1 = new ArrayList<>();
        for (int k = 0; k < result.length; k++) {
            result1.add(result[k]);
        }

        Sequence res = new Sequence(result1);
        return res;
    }

    public Sequence buildSequence(String id, int[] arr, int[] arr2)throws BuilderException {
            String idRet = "";
            Sequence result = new Sequence();
            //  SortClient client = null;
            switch (id) {
                case "One":
                    FirstBuilder firstBuilder=new FirstBuilder();
                    result= firstBuilder.buildSequence(arr, arr2);
                    idRet = "One";
                    break;

                case "Two":
                    SecondBuilder secondBuilder= new SecondBuilder();
                    result= secondBuilder.buildSequence(arr, arr2);
                    idRet = "Two";
                    break;

                case "Three":
                    ThirdBuilder thirdBuilder=new ThirdBuilder();
                    result=thirdBuilder.buildSequence(arr, arr2);
                    idRet = "Three";
                    break;
            }
            if (idRet.equals("")) { throw new BuilderException("This is not the sort");}
            return result;
        }

}