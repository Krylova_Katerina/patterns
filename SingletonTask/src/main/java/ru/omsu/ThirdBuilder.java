package ru.omsu;

public class ThirdBuilder extends SequenceBuilder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        PastSort past=new PastSort();
        arr= past.beSort(arr);

        BucketSort bucket = new BucketSort();
        arr2= bucket.beSort(arr2);

        Sequence result= createNewSequenceProduct(arr, arr2);
        return result;
    }
}