package ru.omsu;

public class BuilderDirector {
   protected SequenceBuilder sequenceBuilder;
   protected Sequence sequence;

   private static BuilderDirector instance;

    private BuilderDirector(Sequence sequence) {
        this.sequence = sequence;
    }


     private Sequence constructSequence(String id, int[] arr, int[] arr2) throws BuilderException {
     Sequence sequence= sequenceBuilder.buildSequence(id, arr, arr2);
     getInstances(sequence);
     return sequence;

    }

    public static synchronized BuilderDirector getInstances(Sequence sequence){
        if(instance == null){
            instance = new BuilderDirector(sequence);
        }
        return instance;
    }

}

