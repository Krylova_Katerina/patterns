package ru.omsu;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.AssertJUnit.assertEquals;

public class PrototypeTest {


    @Test
    public void writeReadFileTest(){
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);
        Sequence sequence1=new Sequence();
        sequence1.writeToFile(sequence, "D:\\SomeDir\\input.txt");
        Sequence newSequence = sequence1.readFromFile("D:\\SomeDir\\input.txt");
       // for (int i=0)
        assertEquals(sequence,newSequence);

    }

    @Test
    public void getCacheTest() throws CloneNotSupportedException {
        Map<String, Sequence> map= new HashMap<>();
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);
        map.put("one", sequence);

        List<Integer> list2 = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        list.add(4);
        list.add(7);
        list.add(3);
        list.add(15);
        Sequence sequence2 = new Sequence(list2);
        map.put("two", sequence2);

        List<Integer> list3 = new ArrayList<Integer>();
        list.add(2);
        list.add(15);
        list.add(4);
        list.add(135);
        list.add(-5);
        Sequence sequence3 = new Sequence(list3);
        map.put("three", sequence3);

        SequenceCache sequenceCache = new SequenceCache(map);
        Sequence newSequence= sequenceCache.getSequense("one");
        assertEquals(sequence,newSequence);

    }


    @Test
    public void getCacheTest2() throws CloneNotSupportedException {
        Map<String, Sequence> map= new HashMap<>();
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);
        map.put("one", sequence);
        List<Integer> list2 = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        list.add(4);
        list.add(7);
        list.add(3);
        list.add(15);
        Sequence sequence2 = new Sequence(list2);
        map.put("two", sequence2);
        List<Integer> list3 = new ArrayList<Integer>();
        list.add(2);
        list.add(15);
        list.add(4);
        list.add(135);
        list.add(-5);
        Sequence sequence3 = new Sequence(list3);
        map.put("three", sequence3);

        SequenceCache sequenceCache2 = new SequenceCache(map);
        SequenceCache sequenceCache = new SequenceCache(map);
        Sequence newSequence= sequenceCache.getSequense("one");


        assertEquals(sequenceCache2, sequenceCache);

    }



    @Test
    public void newSequenseWithoutMinMax() throws CloneNotSupportedException {
        Map<String, Sequence> map= new HashMap<>();
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);
        map.put("one", sequence);

        List<Integer> list2 = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        list.add(4);
        list.add(7);
        list.add(3);
        list.add(15);
        Sequence sequence2 = new Sequence(list2);
        map.put("two", sequence2);

        List<Integer> list3 = new ArrayList<Integer>();
        list.add(2);
        list.add(15);
        list.add(4);
        list.add(135);
        list.add(-5);
        Sequence sequence3 = new Sequence(list3);
        map.put("three", sequence3);

        SequenceCache sequenceCache = new SequenceCache(map);
        Sequence newSequence= sequenceCache.newSequenseWithoutMinMax("one", sequenceCache);

        List<Integer> nlist = new ArrayList<Integer>();
        list.add(2);
        list.add(3);
        Sequence nsequence = new Sequence(list);
        assertEquals(nsequence,newSequence);

    }


    @Test
    public void newSequenseWithoutMinMax2() throws CloneNotSupportedException {
        Map<String, Sequence> map= new HashMap<>();
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);
        Sequence sequence = new Sequence(list);
        map.put("one", sequence);

        List<Integer> list2 = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        list.add(4);
        list.add(7);
        list.add(3);
        list.add(15);
        Sequence sequence2 = new Sequence(list2);
        map.put("two", sequence2);

        List<Integer> list3 = new ArrayList<Integer>();
        list.add(2);
        list.add(15);
        list.add(4);
        list.add(135);
        list.add(-5);
        Sequence sequence3 = new Sequence(list3);
        map.put("three", sequence3);

        SequenceCache sequenceCache = new SequenceCache(map);
        Sequence newSequence= sequenceCache.newSequenseWithoutEvenNumbers("one", sequenceCache);

        List<Integer> nlist = new ArrayList<Integer>();
        list.add(1);
        list.add(5);
        list.add(3);
        Sequence nsequence = new Sequence(list);
        assertEquals(nsequence,newSequence);

    }
}
