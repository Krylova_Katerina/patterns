
package ru.omsu;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Sequence implements Cloneable {
    private List<Integer> arr;


    public Sequence() {
    }

    public Sequence(List<Integer> arr) {
        this.arr = arr;
    }

    public List<Integer> getArr() {
        return arr;
    }

    public void setArr(List<Integer> arr) {
        this.arr = arr;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sequence sequence = (Sequence) o;

        return arr != null ? arr.equals(sequence.arr) : sequence.arr == null;
    }

    @Override
    public int hashCode() {
        return arr != null ? arr.hashCode() : 0;
    }

    public void addSequense(int index, Integer el){
        arr.add(index, el);
    }



    public Sequence readFromFile(String fileName) {
        List<Integer> str = new ArrayList<>();
        str.add(1);
        Sequence sequence = new Sequence(str);
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String[] ostr = br.readLine().split(" ");
            for (int i=0; i<ostr.length; i++){
                sequence.getArr().add(i,Integer.valueOf(ostr[i]));
            }
           // int index = sequence.getArr().
            sequence.getArr().remove(sequence.getArr().lastIndexOf(1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sequence;
    }


    public void writeToFile(Sequence sequence, String fileName){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            for (int i=0; i<sequence.getArr().size(); i++){
                bw.write(String.valueOf(sequence.getArr().get(i)));
                bw.write(" ");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


    public Sequence clone() throws CloneNotSupportedException {
        Sequence copy = (Sequence) super.clone();
        return copy;
    }
}