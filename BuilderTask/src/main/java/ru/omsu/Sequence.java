package ru.omsu;

/**
 * Created by User on 09.09.2017.
 */
public class Sequence {
    private int[]arr;

    public Sequence(int[] arr) {
        this.arr = arr;
    }


    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }
}
