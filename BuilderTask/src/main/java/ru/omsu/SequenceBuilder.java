package ru.omsu;

/**
 * Created by User on 09.09.2017.
 */
public abstract class SequenceBuilder {
    private Sequence sequence;

    public Sequence getSequence() {
        return sequence;
    }

    public Sequence createNewSequenceProduct(int[] arr, int[] arr2) {

// Динамически выделяем память под хранение массива, полученного слиянием двух исходных, его размер, очевидно, равен n + m
        int[] result = new int[arr.length + arr2.length];
// Заведем два индекса, указывающих на первый необработанный элемент первого и второго массивов int i = 0, j = 0;
// И заведем индекс массива-результата, который указывает позицию, которая будет заполнена на текущем шаге
// Будем повторять сравнение элементов массивов a и b до тех пор, пока в каждом из них есть хотя бы один
// необработанный элемен
        int i = 0, j = 0, index = 0;
        while (i < arr.length && j < arr2.length) {
// В соответствии с тем, текущий элемент какого массива меньше, мы записываем этот элемент в массив-результат и
// обновляем нужный индекс (i или j)
            if (arr[i] < arr2[j]) {
                result[index] = arr[i];
                i++;
            } else {
                result[index] = arr2[j];
                j++;
            }
            index++;
        }
// После выполнения предыдущего цикла все элементы одного из массивов будут обработаны, тогда оставшиеся элементы
// другого массива нужно просто дописать в массив-результат Заметим, что одно из условий (i < n) или (j < m)
// будет гарантированно ложно

        while (i < arr.length) {
            result[index] = arr[i];
            index++;
            i++;
        }
        while (j < arr2.length) {
            result[index] = arr2[j];
            index++;
            j++;
        }
        // Выводим отсортированный массив
        Sequence res =new Sequence(result);
        return res;
    }

    public abstract Sequence buildSequence(int[] arr, int[] arr2);
}
