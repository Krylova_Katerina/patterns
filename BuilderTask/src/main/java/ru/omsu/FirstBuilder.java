package ru.omsu;

/**
 * Created by User on 10.09.2017.
 */
public class FirstBuilder extends SequenceBuilder {
    @Override
    public Sequence buildSequence(int[] arr, int[] arr2) {
        BucketSort bucket = new BucketSort();
        arr= bucket.beSort(arr);

        FastSort fast = new FastSort();
        arr2 = fast.beSort(arr2);

        Sequence result= createNewSequenceProduct(arr, arr2);
        return result;
    }
}
