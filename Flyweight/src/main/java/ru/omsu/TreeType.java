package ru.omsu;

import java.awt.*;

public class TreeType {
    private String name;
    private Color color;
    private String description;

    public TreeType(String name, Color color, String description) {
        this.name = name;
        this.color = color;
        this.description = description;
    }

    public void draw(Graphics g, int x, int y) {
        g.setColor(Color.BLACK);
        g.fillRect(x - 3, y, 5, 7);
        g.setColor(color);
        g.fillOval(x - 7, y - 12, 12, 12);
    }
}