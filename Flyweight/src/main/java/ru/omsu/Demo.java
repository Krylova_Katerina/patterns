package ru.omsu;

import java.awt.*;

public class Demo {
    static int SIZE = 500;
    static int SIZE_W = 700;
    static int NUMBER = 5000;
    static int TYPES = 4;

    public static void main(String[] args) {
        Forest forest = new Forest();
        for (int i = 0; i < Math.floor(NUMBER / TYPES); i++) {
            forest.plantTree(random(0, SIZE_W), random(0, SIZE),
                    "Summer tree", Color.GREEN, "Summer texture tree");
            forest.plantTree(random(0, SIZE_W), random(0, SIZE),
                    "Autumn tree", Color.ORANGE, "Autumn texture tree");
            forest.plantTree(random(0, SIZE_W), random(0, SIZE),
                    "Winter tree", Color.CYAN, "Winter texture tree");
            forest.plantTree(random(0, SIZE_W), random(0, SIZE),
                    "Spring tree", Color.MAGENTA, "Spring texture tree");
        }
        forest.setSize(SIZE_W, SIZE);
        forest.setVisible(true);

    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}