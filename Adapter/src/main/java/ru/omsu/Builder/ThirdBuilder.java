package ru.omsu.Builder;

import ru.omsu.Sequence;
import ru.omsu.SequenceBuilder;
import ru.omsu.Sort.*;

public class ThirdBuilder extends Builder implements SequenceBuilder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        PastSort past=new PastSort();
        arr= past.beSort(arr);

        BucketSort bucket = new BucketSort();
        arr2= bucket.beSort(arr2);

        Sequence result= createNewSequenceProduct(arr, arr2);
        return result;
    }


}