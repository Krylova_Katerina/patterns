package ru.omsu.Builder;

import ru.omsu.Sequence;
import ru.omsu.SequenceBuilder;

import java.util.ArrayList;
import java.util.List;

public class Builder implements SequenceBuilder {

    protected Sequence sequence;
    public Sequence getSequence() {
        return sequence;
    }

    public Sequence createNewSequenceProduct(int[] arr, int[] arr2) {
        int[] result = new int[arr.length + arr2.length];
        int i = 0, j = 0, index = 0;
        while (i < arr.length && j < arr2.length) {
            if (arr[i] < arr2[j]) {
                result[index] = arr[i];
                i++;
            } else {
                result[index] = arr2[j];
                j++;
            }
            index++;
        }
        while (i < arr.length) {
            result[index] = arr[i];
            index++;
            i++;
        }
        while (j < arr2.length) {
            result[index] = arr2[j];
            index++;
            j++;
        }


        List<Integer> result1 = new ArrayList<>();
        for (int k = 0; k < result.length; k++) {
            result1.add(result[k]);
        }

        Sequence res = new Sequence(result1);
        return res;
    }

    public static Sequence buildSequence(String id, int[] arr, int[] arr2)throws BuilderException {
        String idRet = "";
        Sequence result = new Sequence();
        //  SortClient client = null;
        switch (id) {
            case "one":
                FirstBuilder firstBuilder=new FirstBuilder();
                result= firstBuilder.buildSequence(arr, arr2);
                idRet = "one";
                break;

            case "two":
                SecondBuilder secondBuilder= new SecondBuilder();
                result= secondBuilder.buildSequence(arr, arr2);
                idRet = "two";
                break;

            case "three":
                ThirdBuilder thirdBuilder=new ThirdBuilder();
                result=thirdBuilder.buildSequence(arr, arr2);
                idRet = "three";
                break;
        }
        if (idRet.equals("")) { throw new BuilderException("This is not the sort");}
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Builder builder = (Builder) o;

        return sequence != null ? sequence.equals(builder.sequence) : builder.sequence == null;
    }

    @Override
    public int hashCode() {
        return sequence != null ? sequence.hashCode() : 0;
    }
}
