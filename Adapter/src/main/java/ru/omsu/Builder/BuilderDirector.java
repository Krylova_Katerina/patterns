package ru.omsu.Builder;

import ru.omsu.Sequence;
import ru.omsu.SequenceBuilder;

import static ru.omsu.Builder.Builder.buildSequence;

public class BuilderDirector {
   protected SequenceBuilder sequenceBuilder;
   protected Sequence sequence;

   private static BuilderDirector instance;
/*
    private BuilderDirector(Sequence sequence) throws BuilderException {
        this.sequence = sequence;
    }

    private Sequence constructSequence(String id, int[] arr, int[] arr2) throws BuilderException {
     Sequence sequence= sequenceBuilder.buildSequence(id, arr, arr2);
     return sequence;
    }

    public static synchronized BuilderDirector getInstances(Sequence sequence) throws BuilderException {
        if(instance == null){
            instance = new BuilderDirector(sequence);
        }
        return instance;
    }
    */
/*
    public  BuilderDirector (String id, int[] arr, int[] arr2) throws BuilderException {
        this.sequence = sequenceBuilder.buildSequence(id, arr, arr2);

    }*/

    public static Sequence constructSequence(String id, int[] arr, int[] arr2) throws BuilderException {
        Sequence sequence= buildSequence(id, arr, arr2);
        return sequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuilderDirector that = (BuilderDirector) o;

        if (sequenceBuilder != null ? !sequenceBuilder.equals(that.sequenceBuilder) : that.sequenceBuilder != null)
            return false;
        return sequence != null ? sequence.equals(that.sequence) : that.sequence == null;
    }

    @Override
    public int hashCode() {
        int result = sequenceBuilder != null ? sequenceBuilder.hashCode() : 0;
        result = 31 * result + (sequence != null ? sequence.hashCode() : 0);
        return result;
    }
}

