
package ru.omsu;

import ru.omsu.Builder.BuilderException;
import ru.omsu.Builder.FirstBuilder;
import ru.omsu.Builder.SecondBuilder;
import ru.omsu.Builder.ThirdBuilder;

/**
 * Created by Student on 15.09.17.
 */
public interface SequenceBuilder {

    public Sequence getSequence();

    public Sequence createNewSequenceProduct(int[] arr, int[] arr2);

    public static Sequence buildSequence(String id, int[] arr, int[] arr2)throws BuilderException{
        String idRet = "";
        Sequence result = new Sequence();
        //  SortClient client = null;
        switch (id) {
            case "one":
                FirstBuilder firstBuilder=new FirstBuilder();
                result= firstBuilder.buildSequence(arr, arr2);
                idRet = "one";
                break;

            case "two":
                SecondBuilder secondBuilder= new SecondBuilder();
                result= secondBuilder.buildSequence(arr, arr2);
                idRet = "two";
                break;

            case "three":
                ThirdBuilder thirdBuilder=new ThirdBuilder();
                result=thirdBuilder.buildSequence(arr, arr2);
                idRet = "three";
                break;
        }
        if (idRet.equals("")) { throw new BuilderException("This is not the sort");}
        return result;
    }

}