package ru.omsu;

import java.util.Map;

public class SequenceCache {
    private  Map<String, Sequence> cache;

    public SequenceCache(Map<String, Sequence> cache) {
        this.cache = cache;
    }

    public  Map<String, Sequence> getCache() {
        return cache;
    }


    public Sequence getSequence(String name) throws CloneNotSupportedException {
        Sequence sequence =null;
        if(cache.containsKey(name)&&cache.get(name)!=null){
            sequence=cache.get(name).clone();
        }
        return  sequence;
    }

    public Sequence newSequenceWithoutMinMax(String name, SequenceCache sequenceMap) throws CloneNotSupportedException {
        Sequence sequence = sequenceMap.getSequence(name);
        int max = sequence.getArr().get(0);
        int min = sequence.getArr().get(0);
        for (int i=0; i<sequence.getArr().size();i++){
            if (sequence.getArr().get(i)<min){ min = sequence.getArr().get(i);}
            if (sequence.getArr().get(i)>max){ max = sequence.getArr().get(i);}
        }
        sequence.getArr().remove(sequence.getArr().indexOf(max));
        sequence.getArr().remove(sequence.getArr().indexOf(min));
        return sequence;

    }

    public Sequence newSequenceWithoutEvenNumbers(String name, SequenceCache sequenceMap) throws CloneNotSupportedException {
        Sequence sequence = sequenceMap.getSequence(name);
        int i=0;
        while (sequence.getArr().size()>i){
            if (sequence.getArr().get(i)%2 == 0){sequence.getArr().remove(i);
            i--;}
            else i++;
        }
        return sequence;

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SequenceCache that = (SequenceCache) o;

        return cache != null ? cache.equals(that.cache) : that.cache == null;
    }

    @Override
    public int hashCode() {
        return cache != null ? cache.hashCode() : 0;
    }
}
