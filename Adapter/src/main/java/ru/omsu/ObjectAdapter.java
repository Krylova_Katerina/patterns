package ru.omsu;

import ru.omsu.Builder.Builder;
import ru.omsu.Builder.BuilderDirector;
import ru.omsu.Builder.BuilderException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static ru.omsu.Builder.BuilderDirector.constructSequence;

public class ObjectAdapter extends Builder implements SequenceBuilder{
    private static BuilderDirector builderDirector;


    public static String[] readMessege(String fileName) {
        String[] ostr = new String[0];
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            ostr = br.readLine().split(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ostr;
    }

    public static Sequence messegeToString(String fileName, int[] arr, int[] arr2) throws AdapterExeption, BuilderException {
        String[] ostr= readMessege(fileName);
        String id = "";
        if ((ostr[ostr.length-1].codePointAt(0) > 48) &&(ostr[ostr.length-1].codePointAt(0) < 57)){
            switch (ostr[ostr.length-1].codePointAt(0)) {
                case 49:
                    id= "one";
                    break;

                case 50:
                    id= "two";
                    break;

                case 51:
                    id="three";
                    break;
            }
            if (id.equals("")){
                throw new AdapterExeption("This is not the sort");
            }
        }
        else {
             id = ostr[ostr.length-1];
        }
        Sequence sequence= constructSequence(id, arr, arr2);
        return sequence;
    }




  /*
    public Sequence build (String fileName, int[] arr, int[] arr2) throws AdapterExeption, BuilderException {
        String messege = messegeToString(readMessege(fileName));
        Sequence sequence= builderDirector.constructSequence(messege, arr, arr2);
        return  sequence;
    }*/

}
