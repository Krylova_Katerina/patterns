/**
 * Created by Student on 15.09.17.
 */
package ru.omsu.Sort;

public class PastSort implements Sort {
    public int[] beSort(int[] arr) {
        int temp, j;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i + 1];
                arr[i + 1] = arr[i];
                j = i;
                while (j > 0 && temp < arr[j - 1]) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = temp;
            }
        }
        for (j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + "  ");
        }
        return arr;
    }
}
