package ru.omsu;

import org.junit.Test;
import ru.omsu.Builder.FirstBuilder;
import ru.omsu.Builder.SecondBuilder;
import ru.omsu.Builder.ThirdBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class SingletonTest {

        @Test
    public void StrategyTest(){
        int[] arr= new int[]{3, 2, 1, 4, 8};
        int[] arr2= new int[]{5, 4, 2, 1, 4};
        Sequence result= new Sequence(new FirstBuilder());
        result= result.buildSequence(arr,arr2);

        List<Integer> list = new ArrayList();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        list.add(4);
        list.add(5);
        list.add(8);

        Sequence expect = new Sequence(list);
        assertEquals(result,expect);

    }

    @Test
    public void StrategyTest2(){
        int[] arr= new int[]{3, 2, 1, 4, 8};
        int[] arr2= new int[]{5, 4, 2, 1, 4};
        Sequence result= new Sequence(new SecondBuilder());
        result= result.buildSequence(arr,arr2);

        List<Integer> list = new ArrayList();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        list.add(4);
        list.add(5);
        list.add(8);

        Sequence expect = new Sequence(list);
        assertEquals(result,expect);

    }


    @Test
    public void StrategyTest3(){
        int[] arr= new int[]{3, 2, 1, 4, 8};
        int[] arr2= new int[]{5, 4, 2, 1, 4};
        Sequence result= new Sequence(new ThirdBuilder());
        result= result.buildSequence(arr,arr2);

        List<Integer> list = new ArrayList();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        list.add(4);
        list.add(5);
        list.add(8);

        Sequence expect = new Sequence(list);
        assertEquals(result,expect);

    }

    }







