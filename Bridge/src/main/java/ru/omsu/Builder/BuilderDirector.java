package ru.omsu.Builder;

import ru.omsu.Sequence;

public interface BuilderDirector {
    Sequence buildSequence(int[] arr, int[] arr2);
/*   protected SequenceBuilder sequenceBuilder;
   protected Sequence sequence;

   private static BuilderDirector instance;

    public static Sequence constructSequence(String id, int[] arr, int[] arr2) throws BuilderException {
        Sequence sequence= buildSequence(id, arr, arr2);
        return sequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuilderDirector that = (BuilderDirector) o;

        if (sequenceBuilder != null ? !sequenceBuilder.equals(that.sequenceBuilder) : that.sequenceBuilder != null)
            return false;
        return sequence != null ? sequence.equals(that.sequence) : that.sequence == null;
    }

    @Override
    public int hashCode() {
        int result = sequenceBuilder != null ? sequenceBuilder.hashCode() : 0;
        result = 31 * result + (sequence != null ? sequence.hashCode() : 0);
        return result;
    }*/

}

