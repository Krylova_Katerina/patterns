package ru.omsu.Builder;

import ru.omsu.Sequence;

import java.util.ArrayList;
import java.util.List;

public abstract class Builder implements BuilderDirector {
    BuilderDirector builderDirector;

    public Builder(BuilderDirector builderDirector) {
        this.builderDirector = builderDirector;
    }

    protected Builder() {
    }

    public Sequence buildSequence( int[] arr, int[] arr2){
        Sequence sequence= builderDirector.buildSequence(arr, arr2);
        return sequence;
    }

    public Sequence createNewSequenceProduct(int[] arr, int[] arr2) {
        int[] result = new int[arr.length + arr2.length];
        int i = 0, j = 0, index = 0;
        while (i < arr.length && j < arr2.length) {
            if (arr[i] < arr2[j]) {
                result[index] = arr[i];
                i++;
            } else {
                result[index] = arr2[j];
                j++;
            }
            index++;
        }
        while (i < arr.length) {
            result[index] = arr[i];
            index++;
            i++;
        }
        while (j < arr2.length) {
            result[index] = arr2[j];
            index++;
            j++;
        }
        List<Integer> result1 = new ArrayList<>();
        for (int k = 0; k < result.length; k++) {
            result1.add(result[k]);
        }
        Sequence res = new Sequence(result1);
        return res;
    }


}

