package ru.omsu.Builder;


import ru.omsu.Sequence;
import ru.omsu.Sort.FastSort;
import ru.omsu.Sort.PastSort;

public class SecondBuilder extends Builder {

    public Sequence buildSequence(int[] arr, int[] arr2) {
        PastSort past=new PastSort();
        arr= past.beSort(arr);

        FastSort fast = new FastSort();
        arr2 = fast.beSort(arr2);

        Sequence result= createNewSequenceProduct(arr, arr2);
        return result;
    }


}