
package ru.omsu;

import ru.omsu.Builder.Builder;
import ru.omsu.Builder.BuilderDirector;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Sequence extends Builder implements Cloneable {
   private List<Integer> arr;

    public Sequence(List<Integer> arr) {
        this.arr = arr;
    }


    public Sequence(BuilderDirector builderDirector) {
        super(builderDirector);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sequence sequence = (Sequence) o;

        return arr.equals(sequence.arr);
    }

    @Override
    public int hashCode() {
        return arr.hashCode();
    }

    public List<Integer> getArr() {
        return arr;
    }

    public Sequence readFromFile(String fileName) {
        List<Integer> str = new ArrayList<>();
        str.add(1);
        Sequence sequence = new Sequence(str);
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String[] ostr = br.readLine().split(" ");
            for (int i=0; i<ostr.length; i++){
                sequence.getArr().add(i,Integer.valueOf(ostr[i]));
            }
           // int index = sequence.getArr().
            sequence.getArr().remove(sequence.getArr().lastIndexOf(1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sequence;
    }


    public void writeToFile(Sequence sequence, String fileName){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            for (int i=0; i<sequence.getArr().size(); i++){
                bw.write(String.valueOf(sequence.getArr().get(i)));
                bw.write(" ");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


    public Sequence clone() throws CloneNotSupportedException {
        Sequence copy = (Sequence) super.clone();
        return copy;
    }
}