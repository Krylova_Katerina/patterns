/**
 * Created by Student on 02.09.2017.
 */
package ru.omsu;

public class Factory {
    public String factoryMethood(String id, int[] arr) throws FactorExeption {
        String idRet = "";
        switch (id) {
            case "Past":
                PastSort past = new PastSort();
                past.beSort(arr);
                idRet = "Past";
                break;

            case "Fast":
                FastSort fast = new FastSort();
                fast.beSort(arr);
                idRet = "Fast";
                break;

            case "Bucket":
                BucketSort bucket = new BucketSort();
                bucket.beSort(arr);
                idRet = "Bucket";
                break;
        }
        if (idRet.equals("")) { throw new FactorExeption("This is not the sort");}
        return idRet;
    }
}
