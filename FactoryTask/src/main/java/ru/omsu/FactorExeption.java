package ru.omsu;

/**
 * Created by User on 08.09.2017.
 */
public class FactorExeption extends Throwable {
    public FactorExeption() {
    }

    public FactorExeption(String message) {
        super(message);
    }

    public FactorExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public FactorExeption(Throwable cause) {
        super(cause);
    }

    public FactorExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
