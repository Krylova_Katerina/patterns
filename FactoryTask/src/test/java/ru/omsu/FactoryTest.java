package ru.omsu;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Student on 02.09.2017.
 */
public class FactoryTest {

    @Test
    public void factoryMethoodPastTest() throws FactorExeption {
        Factory factory = new Factory();
        int[] arr= new int[]{1, 1, 2, 43, 63, 126, 12, 24};
        String tmp = factory.factoryMethood("Past",arr);
        assertEquals("Past", tmp);
    }

    @Test(expected = FactorExeption.class)
    public void factoryMethoodTest2() throws FactorExeption {
        Factory factory = new Factory();
        int[] arr= new int[]{1, 1, 2, 43, 63, 126, 12, 24};
        String tmp = factory.factoryMethood("sdfsedf",arr);
        assertEquals("", tmp);
    }

    @Test
    public void factoryMethoodFastTest3() throws FactorExeption {
        Factory factory = new Factory();
        int[] arr= new int[]{1, 1, 2, 43, 63, 126, 12, 24};
        String tmp = factory.factoryMethood("Fast",arr);
        assertEquals("Fast", tmp);
    }



    @Test
    public void factoryMethoodBucketTest3() throws FactorExeption {
        Factory factory = new Factory();
        int[] arr= new int[]{1, 1, 2, 43, 63, 126, 12, 24};
        String tmp = factory.factoryMethood("Bucket",arr);
        assertEquals("Bucket", tmp);
    }
}
